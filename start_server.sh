#!/bin/bash

# Set the port to use from LISTEN_PORT env variable or 8888 by default
PORT=${LISTEN_PORT:-8888}

TOKEN_PREFIX=${TOKEN_NAME:-MyServer}
PARAM="$1"
SIZE=${PARAM:-6}

# Log file to use for all outputs (STDOUT, STDERR)
LOG_FILE=${LOG_DIR:-/tmp}/access.log

OK=0

# While no SIGTERM launch a server listening
while [ ${OK} -eq 0 ] ; do
	TOKEN="${TOKEN_PREFIX}$(rand-keygen -s ${SIZE})"
	echo "Server ${TOKEN} starting..." | tee -a ${LOG_FILE}

	# Start server listening with netcat
	nc -vlkp ${PORT} 2>&1 | tee -a ${LOG_FILE}
	OK=$?

	echo "Server ${TOKEN} closed" | tee -a ${LOG_FILE}
done
