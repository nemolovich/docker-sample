#!/bin/bash

# Method to randomly generate a name as pattern [A-Z][a-z]{size}[0-9]{2}
# with size got from first parameter
# eg. with size=11: Korbendallas55
get_rand_name()
{
	echo "$(rand-keygen -u -s 1)$(rand-keygen -l -s $1)$(rand-keygen -n -s 2)"
}

# Print 'Hello <RANDOM_NAME>'
echo "Hello $(get_rand_name $1)"
