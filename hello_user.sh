#!/bin/bash

# Print 'Hello <USERNAME>
#        Token: <RANDOMKEY>'
# With system user name got from whoami command
echo "Hello $(whoami)
Token: $(rand-keygen -s $1)"
