
var COPYRIGHTS = 'Copyrights © Capgemini 2016. All Rights Reserved';
// More info about config & dependencies:
// - https://github.com/hakimel/reveal.js#configuration
// - https://github.com/hakimel/reveal.js#dependencies
Reveal.initialize({
	dependencies: [
		{ src: 'lib/js/classList.js', condition: function() { return !document.body.classList; } },
		{ src: 'plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
		{ src: 'plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
		{ src: 'plugin/notes/notes.js', async: true },
		{ src: 'socket.io/socket.io.js', async: true },
		{ src: 'plugin/notes-server/client.js', async: true },
		{ src: 'plugin/zoom-js/zoom.js', async: true },
		{ src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },
	],
	markdown: {
		smartypants: true
	},
	width: 960,
	height: 674.5,
	// Display controls in the bottom right corner
	controls: true,

	// Display a presentation progress bar
	progress: true,

	// Set default timing of 2 minutes per slide
	defaultTiming: 120,

	// Display the page number of the current slide
	slideNumber: 'c',

	// Push each slide change to the browser history
	history: true,

	// Enable keyboard shortcuts for navigation
	keyboard: true,

	// Enable the slide overview mode
	overview: true,

	// Vertical centering of slides
	center: true,

	// Enables touch navigation on devices with touch input
	touch: true,

	// Loop the presentation
	loop: false,

	// Change the presentation direction to be RTL
	rtl: false,

	// Randomizes the order of slides each time the presentation loads
	shuffle: false,

	// Turns fragments on and off globally
	fragments: true,

	// Flags if the presentation is running in an embedded mode,
	// i.e. contained within a limited portion of the screen
	embedded: false,

	// Flags if we should show a help overlay when the questionmark
	// key is pressed
	help: true,

	// Flags if speaker notes should be visible to all viewers
	showNotes: false,

	// Global override for autolaying embedded media (video/audio/iframe)
	// - null: Media will only autoplay if data-autoplay is present
	// - true: All media will autoplay, regardless of individual setting
	// - false: No media will autoplay, regardless of individual setting
	autoPlayMedia: null,

	// Number of milliseconds between automatically proceeding to the
	// next slide, disabled when set to 0, this value can be overwritten
	// by using a data-autoslide attribute on your slides
	autoSlide: 0,

	// Stop auto-sliding after user input
	autoSlideStoppable: true,

	// Use this method for navigation when auto-sliding
	autoSlideMethod: Reveal.navigateNext,

	// Enable slide navigation via mouse wheel
	mouseWheel: false,

	// Hides the address bar on mobile devices
	hideAddressBar: true,

	// Opens links in an iframe preview overlay
	previewLinks: true,

	// Transition style
	transition: 'convex', // none/fade/slide/convex/concave/zoom

	// Transition speed
	transitionSpeed: 'default', // default/fast/slow

	// Transition style for full page slide backgrounds
	backgroundTransition: 'convex', // none/fade/slide/convex/concave/zoom

	// Number of slides away from the current that are visible
	viewDistance: 3,

	// Parallax background image
	parallaxBackgroundImage: './img/paral.png', // e.g. "'https://s3.amazonaws.com/hakim-static/reveal-js/reveal-parallax-1.jpg'"

	// Parallax background size
	parallaxBackgroundSize: '3446px 700px', // CSS syntax, e.g. "2100px 900px"

	// Number of pixels to move the parallax background per slide
	// - Calculated automatically unless specified
	// - Set to 0 to disable movement along an axis
	parallaxBackgroundHorizontal: 210,
	parallaxBackgroundVertical: 0,

	// The display mode that will be used to show slides
	display: 'block'
	
});
Reveal.addEventListener( 'ready', function( event ) {
	// event.currentSlide, event.indexh, event.indexv
	var elms = document.getElementsByClassName('current-date');
	
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	if (dd < 10) {
		dd='0'+dd;
	} 
	if (mm < 10) {
		mm='0'+mm;
	} 
	var today = dd+'/'+mm+'/'+yyyy;
	for (var i = 0; i < elms.length; i++) {
		elms[i].innerHTML = today;
	}
	
	
	var controls = document.getElementsByClassName('controls')[0];
	var btn = document.createElement('button')
	btn.className="navigate-start enabled";
	btn.setAttribute('aria-label',"start slide");
	btn.onclick= function() { document.location.href = '#'; };
	controls.appendChild(btn);
	
	
	elms = document.getElementsByTagName('SECTION');
	var section, nodes, elm, wrapper, wrapperCell, botWrapper,
		pageNumber, cprigths, append, moves;
	var content = false;
	var index = 2;
	for (var i = 1; i < elms.length; i++) {
		section = elms[i];
		if (!section.hasAttribute('data-markdown')) {
			continue;
		}
		if (section.className.indexOf('vertical-center') !== -1) {
			nodes = section.childNodes;
			
			wrapper = document.createElement('div');
			wrapper.className = "wrapper";
			wrapperCell = document.createElement('div');
			
			moves = [];
			for (var x = 0; x < nodes.length; x++) {
				elm = nodes[x];
				append = !(typeof(elm) === 'object' && elm.className && elm.className.length > 0 
					&& elm.className.indexOf('title') !== -1);
				if (append) {
					moves.push(elm);
				}
			}
			
			for (var x = 0; x < moves.length; x++) {
				elm = moves[x];
				section.removeChild(elm);
				wrapperCell.appendChild(elm);
			}
			
			wrapper.appendChild(wrapperCell);
			section.appendChild(wrapper);
		}
		
		botWrapper = document.createElement('p');
		pageNumber = document.createElement('span');
		pageNumber.className = 'page-number';
		pageNumber.innerHTML = index;
		cprigths = document.createElement('span');
		cprigths.className = 'copyrights';
		cprigths.innerHTML = COPYRIGHTS;
		botWrapper.appendChild(pageNumber);
		botWrapper.appendChild(cprigths);
		
		section.appendChild(botWrapper);
		
		index++;
	}
	
} );