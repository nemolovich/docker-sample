#!/bin/sh


if [ -z "$(ls ${APP_PATH}/)" ] ; then
    echo "Copying backup files..."
    cp -rp ${BACK_PATH}/* ${APP_PATH}/
    cd ${APP_PATH}
fi
if [ ! -d ${APP_PATH}/node_modules ] ; then
    echo "Installing npm..."
    npm install --no-progress
fi

echo "Starting server..."
npm start $*


