#!/bin/bash

# Method to randomly generate a name as pattern [A-Z][a-z]{6}[0-9]{2}
# eg.: Awattez44
get_rand_name()
{
	echo "$(rand-keygen -u -s 1)$(rand-keygen -l -s 6)$(rand-keygen -n -s 2)"
}

echo "Hello $(get_rand_name)"
