# Docker samples

This project will help you to create a Docker image using common Dockerfile
instructions.

All this tutorial will be explained step by step on each instruction.

## Summary

1. [FROM](#1-from)
2. [LABEL](#2-label)
3. [ADD/COPY](#3-addcopy)
    1. [ADD](#1-add)
    2. [COPY](#2-copy)
4. [RUN](#4-run)
5. [ENTRYPOINT/CMD](#5-entrypointcmd)
    1. [ENTRYPOINT](#1-entrypoint)
    2. [CMD](#2-cmd)
6. [WORKDIR](#6-workdir)
7. [USER](#7-user)
8. [ARG/ENV](#8-argenv)
    1. [ARG](#1-arg)
    2. [ENV](#2-env)
9. [EXPOSE](#9-expose)
10. [VOLUME](#10-volume)
11. [Conclusion](#11-conclusion)

## 1. FROM

### Short description
`FROM` is used to start Dockerfile from existing image.

### Test it
##### Content
Dockerfile: [Dockerfile](./Dockerfile)
```Dockerfile
FROM ubuntu:17.04
```

##### Build

```sh
docker build -t sample-test ./
```

_Note: `docker build`take file **Dockerfile** in the given context but you can
change the Dockerfile to use by using `-f` option._

##### Run

```sh
docker run -ti --name test sample-test
```

It will open a shell from inside the container.
From this shell you can execute commands that will be executed in the container
context.

You can check the OS informations:
```sh
cat /etc/os-release
```

That produces following result:
```sh
NAME="Ubuntu"
VERSION="17.04 (Zesty Zapus)"
ID=ubuntu
ID_LIKE=debian
PRETTY_NAME="Ubuntu 17.04"
VERSION_ID="17.04"
HOME_URL="https://www.ubuntu.com/"
SUPPORT_URL="https://help.ubuntu.com/"
BUG_REPORT_URL="https://bugs.launchpad.net/ubuntu/"
PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
VERSION_CODENAME=zesty
UBUNTU_CODENAME=zesty
```

You can see the expected Ubuntu version 17.04.

Every command executed from the container has only effects inside the container.
Dont worry to execute following command: ☺
```sh
rm -rf --no-preserve-root /
```

## 2. LABEL

### Short description
`LABEL` allow to add information about the image (like maintainer, version...).

### Test it
##### Content
Dockerfile: [Dockerfile1](./Dockerfile1)
```Dockerfile
FROM ubuntu:17.04

LABEL maintainer "Brian GOHIER<brian.gohier@capgemini.com>"
```

##### Build

```sh
docker build -t sample-test1 -f Dockerfile1 ./
```

##### Run

```sh
docker run --name test1 sample-test1
```

You can check information about container using foillowing command:
```sh
docker inspect test1
```

Output:
```js
[
    {
        ...
        "Config": {
            ...
            "Labels": {
                "maintainer": "Brian GOHIER\u003cbrian.gohier@capgemini.com\u003e"
            }
            ...
        },
        ...
    }
]
```

## 3. ADD/COPY

### Short description
`ADD` and `COPY` allow you to add files inside image layer.

#### 1. ADD
`ADD` is used to get files from network and copy it into image (like `wget`).

#### 2. COPY
`COPY` is used to copy local files into image.

### Test it
#### Test1
##### Content
Dockerfile: [Dockerfile2](./Dockerfile2)
```Dockerfile
FROM ubuntu:17.04

LABEL maintainer "Brian GOHIER<brian.gohier@capgemini.com>"

COPY hello_world.sh /root/hello_world.sh
```

hello_world.sh: [hello_world.sh](./hello_world.sh)
```sh
#!/bin/bash

# Print 'Hello World'
echo "Hello World!"
```

##### Build

```sh
docker build -t sample-test2 -f Dockerfile2 ./
```

##### Run

```sh
docker run -ti --name test2 sample-test2
```

You can run script by using:
```sh
/root/hello_world.sh
```

This will ouput:
```sh
Hello World!
```

----
#### Test2
##### Content
Dockerfile: [Dockerfile3](./Dockerfile3)
```Dockerfile
FROM ubuntu:17.04

LABEL maintainer "Brian GOHIER<brian.gohier@capgemini.com>"

COPY hello_random.sh /root/hello_world.sh
```

hello_random.sh: [hello_random.sh](./hello_random.sh)
```sh
#!/bin/bash

# Method to randomly generate a name as pattern [A-Z][a-z]{6}[0-9]{2}
# eg.: Awattez44
get_rand_name()
{
	echo "$(rand-keygen -u -s 1)$(rand-keygen -l -s 6)$(rand-keygen -n -s 2)"
}

echo "Hello $(get_rand_name)"
```

##### Build

```sh
docker build -t sample-test3 -f Dockerfile3 ./
```

##### Run

```sh
docker run -ti --name test3 sample-test3
```

You can run script by using:
```sh
/root/hello_world.sh
```

This will ouput:
```sh
/root/hello_world.sh: line 7: rand-keygen: command not found
/root/hello_world.sh: line 7: rand-keygen: command not found
/root/hello_world.sh: line 7: rand-keygen: command not found
Hello
```

You can see an error from inside the container because the command
`rand-keygen` does not exist.

We have to add it from Internet.

----
#### Test3
##### Content
Dockerfile: [Dockerfile4](./Dockerfile4)
```Dockerfile
FROM ubuntu:17.04

LABEL maintainer "Brian GOHIER<brian.gohier@capgemini.com>"

COPY hello_random.sh /root/hello_world.sh

ADD https://gitlab.com/nemolovich/shellscripts/raw/master/commons/rand-keygen /usr/bin/rand-keygen

```

hello_random.sh: [hello_random.sh](./hello_random.sh)
```sh
#!/bin/bash

# Method to randomly generate a name as pattern [A-Z][a-z]{6}[0-9]{2}
# eg.: Awattez44
get_rand_name()
{
	echo "$(rand-keygen -u -s 1)$(rand-keygen -l -s 6)$(rand-keygen -n -s 2)"
}

echo "Hello $(get_rand_name)"
```

##### Build

```sh
docker build -t sample-test4 -f Dockerfile4 ./
```

##### Run

```sh
docker run -ti --name test4 sample-test4
```

You can run script by using:
```sh
/root/hello_world.sh
```

This will ouput:
```sh
 /root/hello_world.sh
/root/hello_world.sh: line 7: /usr/bin/rand-keygen: Permission denied
/root/hello_world.sh: line 7: /usr/bin/rand-keygen: Permission denied
/root/hello_world.sh: line 7: /usr/bin/rand-keygen: Permission denied
Hello
```

Now the script does not have execution permission. 

We have to run a `chmod` from inside the container.

## 4. RUN
### Short description
`RUN` allow you execute shell command into the image layer.

### Test it
##### Content
Dockerfile: [Dockerfile5](./Dockerfile5)
```Dockerfile
FROM ubuntu:17.04

LABEL maintainer "Brian GOHIER<brian.gohier@capgemini.com>"

COPY hello_random.sh /root/hello_world.sh

ADD https://gitlab.com/nemolovich/shellscripts/raw/master/commons/rand-keygen /usr/bin/rand-keygen

RUN chmod +rx /usr/bin/rand-keygen
```

hello_random.sh: [hello_random.sh](./hello_random.sh)
```sh
#!/bin/bash

# Method to randomly generate a name as pattern [A-Z][a-z]{6}[0-9]{2}
# eg.: Awattez44
get_rand_name()
{
	echo "$(rand-keygen -u -s 1)$(rand-keygen -l -s 6)$(rand-keygen -n -s 2)"
}

echo "Hello $(get_rand_name)"
```

##### Build

```sh
docker build -t sample-test5 -f Dockerfile5 ./
```

##### Run

```sh
docker run -ti --name test5 sample-test5
```

You can run script by using:
```sh
/root/hello_world.sh
```

This will ouput something with random name like:
```sh
Hello Bgohier37
```

Now everything work from the container. However you have to execute it from
inside the container by calling script.

As described in the next section Docker allow to set the script to use when
container is starting.

## 5. ENTRYPOINT/CMD
### Short description
`ENTRYPOINT` and `CMD` are the actions to run when the container is starting.

#### 1. ENTRYPOINT
`ENTRYPOINT` is the main instruction to use for running instruction.

#### 2. CMD
`CMD` can be used as main running command or arguments for `ENTRYPOINT`.

### Test it
#### Test1
##### Content
Dockerfile: [Dockerfile6](./Dockerfile6)
```Dockerfile
FROM ubuntu:17.04

LABEL maintainer "Brian GOHIER<brian.gohier@capgemini.com>"

COPY hello_random.sh /root/hello_world.sh

ADD https://gitlab.com/nemolovich/shellscripts/raw/master/commons/rand-keygen /usr/bin/rand-keygen

RUN chmod +rx /usr/bin/rand-keygen

ENTRYPOINT [ "/root/hello_world.sh" ]
```

hello_random.sh: [hello_random.sh](./hello_random.sh)
```sh
#!/bin/bash

# Method to randomly generate a name as pattern [A-Z][a-z]{6}[0-9]{2}
# eg.: Awattez44
get_rand_name()
{
	echo "$(rand-keygen -u -s 1)$(rand-keygen -l -s 6)$(rand-keygen -n -s 2)"
}

echo "Hello $(get_rand_name)"
```

##### Build

```sh
docker build -t sample-test6 -f Dockerfile6 ./
```

##### Run

```sh
docker run -ti --name test6 sample-test6
```

This will ouput something with random name like:
```sh
Hello Bgohier37
```

As the container has specified command to run, it wont open a shell but
directly run the command. Once the command is terminated, the container stop.

But you can restart it by using start command:
```
docker start -i test6
```

_Note: At each `start` it will run the script so generate a new random name._

Now we will use parameters.

----
#### Test2
##### Content
Dockerfile: [Dockerfile7](./Dockerfile7)
```Dockerfile
FROM ubuntu:17.04

LABEL maintainer "Brian GOHIER<brian.gohier@capgemini.com>"

COPY hello_random_size.sh /root/hello_world.sh

ADD https://gitlab.com/nemolovich/shellscripts/raw/master/commons/rand-keygen /usr/bin/rand-keygen

RUN chmod +rx /usr/bin/rand-keygen

ENTRYPOINT [ "/root/hello_world.sh" ]
```

hello_random_size.sh: [hello_random_size.sh](./hello_random_size.sh)
```sh
#!/bin/bash

# Method to randomly generate a name as pattern [A-Z][a-z]{size}[0-9]{2}
# with size got from first parameter
# eg. with size=11: Korbendallas55
get_rand_name()
{
	echo "$(rand-keygen -u -s 1)$(rand-keygen -l -s $1)$(rand-keygen -n -s 2)"
}

# Print 'Hello <RANDOM_NAME>'
echo "Hello $(get_rand_name $1)"
```

##### Build

```sh
docker build -t sample-test7 -f Dockerfile7 ./
```

##### Run

```sh
docker run -ti --name test7 sample-test7
```

This will ouput something like:
```sh
Hello EError: Please specify the key length
Usage: /usr/bin/rand-keygen [,PARAMETERS]
    Generate a random key (default size=32 with alphanumeric
    characters upper and lower case).
ACTIONS:
    -h|--help               Display this help message
    -l|--lowercase          Include lowercases
    -u|--uppercase          Include uppercases
    -n|--number             Include numbers
    -s|--size <SIZE>        The key length (default=32)32
```

If you start container without parameter it will fail.

_Note: You have to remove container before to re-run container:
`docker rm -vf test7`._

You can add parameters when using `docker run` command as the command line end:
```sh
docker run -ti --name test7 sample-test7 11
```

This will ouput something with random name like:
```sh
Hello Dockersample07
```

You can also specify default parameters by using `CMD`.

----
#### Test3
##### Content
Dockerfile: [Dockerfile8](./Dockerfile8)
```Dockerfile
FROM ubuntu:17.04

LABEL maintainer "Brian GOHIER<brian.gohier@capgemini.com>"

COPY hello_random_size.sh /root/hello_world.sh

ADD https://gitlab.com/nemolovich/shellscripts/raw/master/commons/rand-keygen /usr/bin/rand-keygen

RUN chmod +rx /usr/bin/rand-keygen

ENTRYPOINT [ "/root/hello_world.sh" ]
CMD [ "6" ]
```

hello_random_size.sh: [hello_random_size.sh](./hello_random_size.sh)
```sh
#!/bin/bash

# Method to randomly generate a name as pattern [A-Z][a-z]{size}[0-9]{2}
# with size got from first parameter
# eg. with size=11: Korbendallas55
get_rand_name()
{
	echo "$(rand-keygen -u -s 1)$(rand-keygen -l -s $1)$(rand-keygen -n -s 2)"
}

# Print 'Hello <RANDOM_NAME>'
echo "Hello $(get_rand_name $1)"
```

##### Build

```sh
docker build -t sample-test8 -f Dockerfile8 ./
```

##### Run

```sh
docker run -ti --name test8 sample-test8
```

This will ouput something random like:
```sh
Hello Sdocker08
```

If you dont use parameter it will use default parameter specified in `CMD`
(here 6).

_Note: You have to remove container before to re-run container:
`docker rm -vf test8`._

But you can use another value:
```sh
docker run -ti --name test8 sample-test8 32
```

This with output something random like:
```
Hello Thisdockerpresentationiswonderful00
```

The script is started from absolute path **/root/hello_world.sh**. You can also
use relative path and set the current directory to use for the container as
specified in next section.

## 6. WORKDIR
### Short description
`WORKDIR` define directory to be placed when the container start.

### Test it
#### Test1
##### Content
Dockerfile: [Dockerfile9](./Dockerfile9)
```Dockerfile
FROM ubuntu:17.04

LABEL maintainer "Brian GOHIER<brian.gohier@capgemini.com>"

COPY hello_random_size.sh /root/hello_world.sh

ADD https://gitlab.com/nemolovich/shellscripts/raw/master/commons/rand-keygen /usr/bin/rand-keygen

RUN chmod +rx /usr/bin/rand-keygen

WORKDIR /root

ENTRYPOINT [ "./hello_world.sh" ]
CMD [ "6" ]
```

hello_random_size.sh: [hello_random_size.sh](./hello_random_size.sh)
```sh
#!/bin/bash

# Method to randomly generate a name as pattern [A-Z][a-z]{size}[0-9]{2}
# with size got from first parameter
# eg. with size=11: Korbendallas55
get_rand_name()
{
	echo "$(rand-keygen -u -s 1)$(rand-keygen -l -s $1)$(rand-keygen -n -s 2)"
}

# Print 'Hello <RANDOM_NAME>'
echo "Hello $(get_rand_name $1)"
```

##### Build

```sh
docker build -t sample-test9 -f Dockerfile9 ./
```

##### Run

```sh
docker run -ti --name test9 sample-test9
```

Because the default directory is now **/root** the execution has no error. The
entrypoint `[ "./hello_world.sh" ]` is resolved as */root/hello_world*.

----
#### Test2
##### Content
Dockerfile: [Dockerfile10](./Dockerfile10)
```Dockerfile
FROM ubuntu:17.04

LABEL maintainer "Brian GOHIER<brian.gohier@capgemini.com>"

COPY hello_user.sh /root/hello_world.sh

ADD https://gitlab.com/nemolovich/shellscripts/raw/master/commons/rand-keygen /usr/bin/rand-keygen

RUN chmod +rx /usr/bin/rand-keygen

WORKDIR /root

ENTRYPOINT [ "./hello_world.sh" ]
CMD [ "6" ]
```

hello_user.sh: [hello_user.sh](./hello_user.sh)
```sh
#!/bin/bash

# Print 'Hello <USERNAME>
#        Token: <RANDOMKEY>'
# With system user name got from whoami command
echo "Hello $(whoami)
Token: $(rand-keygen -s $1)"
```

##### Build

```sh
docker build -t sample-test10 -f Dockerfile10 ./
```

##### Run

```sh
docker run -ti --name test10 sample-test10
```

The output is like:
```sh
Hello root
Token: adrien
```

In this example we use `whoami` command to display current user. Here we can see
**root** as user. By default docker is using **root** user but you can use
another one if required.

## 7. USER
### Short description
`USER` define the user that will be used as container runner.

_Note: You can map **uid** form the host machine to **uid** inside the
cointainer._

### Test it
#### Test1
##### Content
Dockerfile: [Dockerfile11](./Dockerfile11)
```Dockerfile
FROM ubuntu:17.04

LABEL maintainer "Brian GOHIER<brian.gohier@capgemini.com>"

COPY hello_user.sh /root/hello_world.sh

ADD https://gitlab.com/nemolovich/shellscripts/raw/master/commons/rand-keygen /usr/bin/rand-keygen

RUN chmod +rx /usr/bin/rand-keygen

WORKDIR /root

USER user

ENTRYPOINT [ "./hello_world.sh" ]
CMD [ "6" ]
```

hello_user.sh: [hello_user.sh](./hello_user.sh)
```sh
#!/bin/bash

# Print 'Hello <USERNAME>
#        Token: <RANDOMKEY>'
# With system user name got from whoami command
echo "Hello $(whoami)
Token: $(rand-keygen -s $1)"
```

##### Build

```sh
docker build -t sample-test11 -f Dockerfile11 ./
```

##### Run

```sh
docker run -ti --name test11 sample-test11
```

This will output:
```sh
/usr/bin/docker-current: Error response from daemon: linux spec user: unable
to find user user: no matching entries in passwd file.
```

This error is thrown because the user **user** cannot be found inside the
container. We have to create it.

----
#### Test2
##### Content
Dockerfile: [Dockerfile12](./Dockerfile12)
```Dockerfile
FROM ubuntu:17.04

LABEL maintainer "Brian GOHIER<brian.gohier@capgemini.com>"

COPY hello_user.sh /root/hello_world.sh

ADD https://gitlab.com/nemolovich/shellscripts/raw/master/commons/rand-keygen /usr/bin/rand-keygen

RUN chmod +rx /usr/bin/rand-keygen

WORKDIR /root

RUN useradd -g 0 -u 111 -M -r user \
  && chmod g+wrx /root

USER user

ENTRYPOINT [ "./hello_world.sh" ]
CMD [ "6" ]
```

hello_user.sh: [hello_user.sh](./hello_user.sh)
```sh
#!/bin/bash

# Print 'Hello <USERNAME>
#        Token: <RANDOMKEY>'
# With system user name got from whoami command
echo "Hello $(whoami)
Token: $(rand-keygen -s $1)"
```

##### Build

```sh
docker build -t sample-test12 -f Dockerfile12 ./
```

##### Run

```sh
docker run -ti --name test12 sample-test12
```

This will output something like:
```sh
Hello user
Token: Gohier
```

## 8. ARG/ENV

### Short description
`ARG` and `ENV` are use to set variables inside image for build or as runtime.

#### 1. ARG
`ARG` is used to set parameter for the image buiding (eg. framework version
dynamically).

#### 2. ENV
`ENV` allow to set environment variables which can be used inside container when
running.

### Test it
#### Test1
##### Content
Dockerfile: [Dockerfile13](./Dockerfile13)
```Dockerfile
FROM ubuntu:17.04

LABEL maintainer "Brian GOHIER<brian.gohier@capgemini.com>"

ARG RUN_SCRIPT=hello_user.sh

COPY ${RUN_SCRIPT} /root/hello_world.sh

ADD https://gitlab.com/nemolovich/shellscripts/raw/master/commons/rand-keygen /usr/bin/rand-keygen

RUN chmod +rx /usr/bin/rand-keygen

WORKDIR /root

RUN useradd -g 0 -u 111 -M -r user \
  && chmod g+wrx /root

USER user

ENTRYPOINT [ "./hello_world.sh" ]
CMD [ "6" ]
```

hello_user.sh: [hello_user.sh](./hello_user.sh)
```sh
#!/bin/bash

# Print 'Hello <USERNAME>
#        Token: <RANDOMKEY>'
# With system user name got from whoami command
echo "Hello $(whoami)
Token: $(rand-keygen -s $1)"
```

##### Build

```sh
docker build -t sample-test13 -f Dockerfile13 ./
```

##### Run

```sh
docker run -ti --name test13 sample-test13
```

This will output something like:
```sh
Hello user
Token: RaNdOm
```

The `ARG` **RUN_SCRIPT** has a default value used if you don't give it as build
phase. To set de build arg value you have to use `--build-arg` parameter in
`docker build` command.

----
#### Test2
##### Content
Dockerfile: [Dockerfile13](./Dockerfile13)
```Dockerfile
FROM ubuntu:17.04

LABEL maintainer "Brian GOHIER<brian.gohier@capgemini.com>"

ARG RUN_SCRIPT=hello_user.sh

COPY ${RUN_SCRIPT} /root/hello_world.sh

ADD https://gitlab.com/nemolovich/shellscripts/raw/master/commons/rand-keygen /usr/bin/rand-keygen

RUN chmod +rx /usr/bin/rand-keygen

WORKDIR /root

RUN useradd -g 0 -u 111 -M -r user \
  && chmod g+wrx /root

USER user

ENTRYPOINT [ "./hello_world.sh" ]
CMD [ "6" ]
```

hello_user_custom.sh: [hello_user_custom.sh](./hello_user_custom.sh)
```sh
#!/bin/bash

# Set token name from TOKEN_NAME env variable or 'Token' by default
TOKEN_DESC=${TOKEN_NAME:-Token}

# Print 'Hello custom <USERNAME>
#        <TOKENNAME>: <RANDOMKEY>'
# With system user name got from whoami command
echo "Hello custom $(whoami)
${TOKEN_DESC}: $(rand-keygen -s $1)"
```

##### Build

```sh
docker build -t sample-test13 -f Dockerfile13 --build-arg RUN_SCRIPT=hello_user_custom.sh ./
```

##### Run

_Note: You have to remove container before to re-run container:
`docker rm -vf test13`._

```sh
docker run -ti --name test13 sample-test13
```

This will output something like:
```sh
Hello custom user
Token: GitLab
```

As we can see the script used is **hello_user_custom.sh**. In this script we use
**TOKEN_DESC** variable that takes **TOKEN_NAME** environment variable value if
exists or "_Token_" value if it does not.

----
#### Test3
##### Content
Dockerfile: [Dockerfile14](./Dockerfile14)
```Dockerfile
FROM ubuntu:17.04

LABEL maintainer "Brian GOHIER<brian.gohier@capgemini.com>"

ARG RUN_SCRIPT=hello_user.sh

ENV TOKEN_NAME="Random Token"

COPY ${RUN_SCRIPT} /root/hello_world.sh

ADD https://gitlab.com/nemolovich/shellscripts/raw/master/commons/rand-keygen /usr/bin/rand-keygen

RUN chmod +rx /usr/bin/rand-keygen

WORKDIR /root

RUN useradd -g 0 -u 111 -M -r user \
  && chmod g+wrx /root

USER user

ENTRYPOINT [ "./hello_world.sh" ]
CMD [ "6" ]
```

hello_user_custom.sh: [hello_user_custom.sh](./hello_user_custom.sh)
```sh
#!/bin/bash

# Set token name from TOKEN_NAME env variable or 'Token' by default
TOKEN_DESC=${TOKEN_NAME:-Token}

# Print 'Hello custom <USERNAME>
#        <TOKENNAME>: <RANDOMKEY>'
# With system user name got from whoami command
echo "Hello custom $(whoami)
${TOKEN_DESC}: $(rand-keygen -s $1)"
```

##### Build

```sh
docker build -t sample-test14 -f Dockerfile14 --build-arg RUN_SCRIPT=hello_user_custom.sh ./
```

##### Run

```sh
docker run -ti --name test14 sample-test14
```

This will output something like:
```sh
Hello custom user
Random Token: DoCkEr
```

The default value of **TOKEN_NAME** variable ("_Random Token_") is used here
because we don't set it in `docker run` command. You can set environment
variable by using `--env` or `-e` parameter in `docker run` command.

----
#### Test4
##### Content
Dockerfile: [Dockerfile14](./Dockerfile14)
```Dockerfile
FROM ubuntu:17.04

LABEL maintainer "Brian GOHIER<brian.gohier@capgemini.com>"

ARG RUN_SCRIPT=hello_user.sh

ENV TOKEN_NAME="Random Token"

COPY ${RUN_SCRIPT} /root/hello_world.sh

ADD https://gitlab.com/nemolovich/shellscripts/raw/master/commons/rand-keygen /usr/bin/rand-keygen

RUN chmod +rx /usr/bin/rand-keygen

WORKDIR /root

RUN useradd -g 0 -u 111 -M -r user \
  && chmod g+wrx /root

USER user

ENTRYPOINT [ "./hello_world.sh" ]
CMD [ "6" ]
```

hello_user_custom.sh: [hello_user_custom.sh](./hello_user_custom.sh)
```sh
#!/bin/bash

# Set token name from TOKEN_NAME env variable or 'Token' by default
TOKEN_DESC=${TOKEN_NAME:-Token}

# Print 'Hello custom <USERNAME>
#        <TOKENNAME>: <RANDOMKEY>'
# With system user name got from whoami command
echo "Hello custom $(whoami)
${TOKEN_DESC}: $(rand-keygen -s $1)"
```

##### Build

Reuse previous image.

##### Run

_Note: You have to remove container before to re-run container:
`docker rm -vf test14`._

```sh
docker run -ti --name test14 -e TOKEN_NAME="Serial Key" sample-test14
```

This will output something like:
```sh
Hello custom user
Serial Key: IsCool
```

The environment varirable given in parameter is used instead of default value.
We can also pass number values in `ENV` to set a port for example.

## 9. EXPOSE

### Short description
`EXPOSE` allow to expose listening ports on container to the host machine.

### Test it
#### Test1
##### Content
Dockerfile: [Dockerfile15](./Dockerfile15)
```Dockerfile
FROM ubuntu:17.04

LABEL maintainer "Brian GOHIER<brian.gohier@capgemini.com>"

ARG RUN_SCRIPT=hello_user.sh

ENV TOKEN_NAME="Random Token"
ENV LISTEN_PORT=5000

COPY ${RUN_SCRIPT} /root/hello_world.sh

ADD https://gitlab.com/nemolovich/shellscripts/raw/master/commons/rand-keygen /usr/bin/rand-keygen

RUN chmod +rx /usr/bin/rand-keygen

WORKDIR /root

RUN useradd -g 0 -u 111 -M -r user \
  && chmod g+wrx /root \
  && apt-get update \
  && apt-get install -y net-tools curl netcat

USER user

ENTRYPOINT [ "./hello_world.sh" ]
CMD [ "6" ]
```

_Note: We use `apt-get` to install some net tools that are not available on the
image of ubuntu used._

start_server.sh: [start_server.sh](./start_server.sh)
```sh
#!/bin/bash

# Set the port to use from LISTEN_PORT env variable or 8888 by default
PORT=${LISTEN_PORT:-8888}

TOKEN_PREFIX=${TOKEN_NAME:-MyServer}
PARAM="$1"
SIZE=${PARAM:-6}

# Log file to use for all outputs (STDOUT, STDERR)
LOG_FILE=${LOG_DIR:-/tmp}/access.log

OK=0

# While no SIGTERM launch a server listening
while [ ${OK} -eq 0 ] ; do
	TOKEN="${TOKEN_PREFIX}$(rand-keygen -s ${SIZE})"
	echo "Server ${TOKEN} starting..." | tee -a ${LOG_FILE}

	# Start server listening with netcat
	nc -vlkp ${PORT} 2>&1 | tee -a ${LOG_FILE}
	OK=$?

	echo "Server ${TOKEN} closed" | tee -a ${LOG_FILE}
done
```

##### Build

```sh
docker build -t sample-test15 -f Dockerfile15 --build-arg RUN_SCRIPT=start_server.sh ./
```

##### Run

```sh
docker run -ti --name test15 -e TOKEN_NAME="Server_" sample-test15
```

This will output something like:
```sh
Server Server_revreS starting...
listening on [any] 5000 ...
```

This start a listening server using **Netcat** inside the container. However if
you try to connect on this port from the host machine you cannot not access to
this server:
```sh
nc -v localhost 5000
```

Output error:
```sh
Ncat: Version 6.40 ( http://nmap.org/ncat )
Ncat: Connection to ::1 failed: Connection refused.
Ncat: Trying next address...
Ncat: Connection refused.
```

Indeed Docker is running container in protected context that does not allow you
to expose port if you did not explicitly configure it.

But you can access on this server from the container itself. You can access to
the container using the following command:
```sh
docker exec -ti test15 /bin/bash
```

And then test to connect and type "_Hello_" then press enter:
```sh
nc -v localhost 5000
```

The client output is:
```
localhost [127.0.0.1] 5000 (?) open
Hello
```

In the server side you can see something like:
```
connect to [127.0.0.1] from localhost [127.0.0.1] 45390
Hello
Server Server_revreS closed
Server Server_Second starting...
listening on [any] 5000 ...
```

The communication is working now from inside the container. If you want use the
container port and expose it on host machine you must use `EXPOSE` instruction.

----
#### Test2
##### Content
Dockerfile: [Dockerfile16](./Dockerfile16)
```Dockerfile
FROM ubuntu:17.04

LABEL maintainer "Brian GOHIER<brian.gohier@capgemini.com>"

ARG RUN_SCRIPT=hello_user.sh

ENV TOKEN_NAME="Random Token"
ARG LISTEN_PORT=5000
ENV LISTEN_PORT=${LISTEN_PORT}

COPY ${RUN_SCRIPT} /root/hello_world.sh

ADD https://gitlab.com/nemolovich/shellscripts/raw/master/commons/rand-keygen /usr/bin/rand-keygen

RUN chmod +rx /usr/bin/rand-keygen

WORKDIR /root

RUN useradd -g 0 -u 111 -M -r user \
  && chmod g+wrx /root \
  && apt-get update \
  && apt-get install -y net-tools curl netcat

USER user

EXPOSE ${LISTEN_PORT}

ENTRYPOINT [ "./hello_world.sh" ]
CMD [ "6" ]
```

start_server.sh: [start_server.sh](./start_server.sh)
```sh
#!/bin/bash

# Set the port to use from LISTEN_PORT env variable or 8888 by default
PORT=${LISTEN_PORT:-8888}

TOKEN_PREFIX=${TOKEN_NAME:-MyServer}
PARAM="$1"
SIZE=${PARAM:-6}

# Log file to use for all outputs (STDOUT, STDERR)
LOG_FILE=${LOG_DIR:-/tmp}/access.log

OK=0

# While no SIGTERM launch a server listening
while [ ${OK} -eq 0 ] ; do
	TOKEN="${TOKEN_PREFIX}$(rand-keygen -s ${SIZE})"
	echo "Server ${TOKEN} starting..." | tee -a ${LOG_FILE}

	# Start server listening with netcat
	nc -vlkp ${PORT} 2>&1 | tee -a ${LOG_FILE}
	OK=$?

	echo "Server ${TOKEN} closed" | tee -a ${LOG_FILE}
done
```

##### Build

```sh
docker build -t sample-test16 -f Dockerfile16 --build-arg RUN_SCRIPT=start_server.sh ./
```

##### Run

```sh
docker run -ti --name test16 -e TOKEN_NAME="Server_" sample-test16
```

Once server start you can try to access from host machine. But this wont work
yet because the port is not bound. You can see the listen port inside the
container using:
```sh
docker ps
```

This will display something like:
```sh
CONTAINER ID        IMAGE                             COMMAND                CREATED             STATUS              PORTS                                 NAMES
a107559e6527        sample-test16                     "./hello_world.sh 6"   11 seconds ago      Up 9 seconds        5000/tcp                              test16
```

We can see information about running containers like image used, start command
and ports. The port 5000 is exposed but not bound.

If you want use the container port and expose it on host machine you must set
the port binding in `docker run` command with `--port` or `-p` parameter.

----
#### Test3
##### Content
Dockerfile: [Dockerfile16](./Dockerfile16)
```Dockerfile
FROM ubuntu:17.04

LABEL maintainer "Brian GOHIER<brian.gohier@capgemini.com>"

ARG RUN_SCRIPT=hello_user.sh

ENV TOKEN_NAME="Random Token"
ARG LISTEN_PORT=5000
ENV LISTEN_PORT=${LISTEN_PORT}

COPY ${RUN_SCRIPT} /root/hello_world.sh

ADD https://gitlab.com/nemolovich/shellscripts/raw/master/commons/rand-keygen /usr/bin/rand-keygen

RUN chmod +rx /usr/bin/rand-keygen

WORKDIR /root

RUN useradd -g 0 -u 111 -M -r user \
  && chmod g+wrx /root \
  && apt-get update \
  && apt-get install -y net-tools curl netcat

USER user

EXPOSE ${LISTEN_PORT}

ENTRYPOINT [ "./hello_world.sh" ]
CMD [ "6" ]
```

start_server.sh: [start_server.sh](./start_server.sh)
```sh
#!/bin/bash

# Set the port to use from LISTEN_PORT env variable or 8888 by default
PORT=${LISTEN_PORT:-8888}

TOKEN_PREFIX=${TOKEN_NAME:-MyServer}
PARAM="$1"
SIZE=${PARAM:-6}

# Log file to use for all outputs (STDOUT, STDERR)
LOG_FILE=${LOG_DIR:-/tmp}/access.log

OK=0

# While no SIGTERM launch a server listening
while [ ${OK} -eq 0 ] ; do
	TOKEN="${TOKEN_PREFIX}$(rand-keygen -s ${SIZE})"
	echo "Server ${TOKEN} starting..." | tee -a ${LOG_FILE}

	# Start server listening with netcat
	nc -vlkp ${PORT} 2>&1 | tee -a ${LOG_FILE}
	OK=$?

	echo "Server ${TOKEN} closed" | tee -a ${LOG_FILE}
done
```

##### Build

Reuse previous image.

##### Run

_Note: You have to remove container before to re-run container:
`docker rm -vf test16`._

```sh
docker run -ti --name test16 -p 8080:5000 -e TOKEN_NAME="Server_" sample-test16
```

_Note: Port binding value is \<HOST_PORT\>:\<CONTAINER_PORT\>. In this example we
bind host port 8080 on the container port 5000._

Once server start you can try to access from host machine on port 8080 an type
"_Hello container!_" then press enter. The output is something like:
```sh
Ncat: Version 6.40 ( http://nmap.org/ncat )
Ncat: Connected to ::1:8080.
Hello container!
```

And in server side:
```sh
Server Server_l1nk3d starting...
listening on [any] 5000 ...
172.17.0.1: inverse host lookup failed: Unknown host
connect to [172.17.0.3] from (UNKNOWN) [172.17.0.1] 47232
Hello container!
Server Server_l1nk3d closed
Server Server_aBcDeF starting...
```

If you check container information:
```sh
docker ps
```

This will display something like:
```sh
CONTAINER ID        IMAGE                             COMMAND                CREATED             STATUS              PORTS                                 NAMES
f07e877be171        sample-test16                     "./hello_world.sh 6"   3 minutes ago       Up 6 seconds        0.0.0.0:8080->5000/tcp                test16
```

We can see the binding between port 8080 on host and 5000 on container.

This uses docker proxy. Use following command:
```sh
netstat -eaplnt | grep -P "Proto|8080"
```

That produces:
```sh
Proto Recv-Q Send-Q Adresse locale          Adresse distante        Etat        Utilisatr  Inode      PID/Program name
tcp6       0      0 :::8080                 :::*                    LISTEN      0          473678     14302/docker-proxy-
```

With `EXPOSE` you can _share_ a port between container and host. But you can
also share files and folders using `VOLUME`.

## 10. VOLUME

### Short description
`VOLUME` allow host machine to bind directories or files to container resources.

### Test it
#### Test1
##### Content
Dockerfile: [Dockerfile17](./Dockerfile17)
```Dockerfile
FROM ubuntu:17.04

LABEL maintainer "Brian GOHIER<brian.gohier@capgemini.com>"

ARG RUN_SCRIPT=hello_user.sh

ENV TOKEN_NAME="Random Token"
ARG LISTEN_PORT=5000
ENV LISTEN_PORT=${LISTEN_PORT}
ENV LOG_DIR=/var/log/nc

COPY ${RUN_SCRIPT} /root/hello_world.sh

ADD https://gitlab.com/nemolovich/shellscripts/raw/master/commons/rand-keygen /usr/bin/rand-keygen

RUN chmod +rx /usr/bin/rand-keygen

WORKDIR /root

RUN useradd -g 0 -u 111 -M -r user \
  && chmod g+wrx /root \
  && apt-get update \
  && apt-get install -y net-tools curl netcat \
  && mkdir -p ${LOG_DIR} \
  && chmod -R 777 ${LOG_DIR}

USER user

EXPOSE ${LISTEN_PORT}

VOLUME ${LOG_DIR}

ENTRYPOINT [ "./hello_world.sh" ]
CMD [ "6" ]
```

_Note: We create directory and give all permissions on this directory in the
image._

start_server.sh: [start_server.sh](./start_server.sh)
```sh
#!/bin/bash

# Set the port to use from LISTEN_PORT env variable or 8888 by default
PORT=${LISTEN_PORT:-8888}

TOKEN_PREFIX=${TOKEN_NAME:-MyServer}
PARAM="$1"
SIZE=${PARAM:-6}

# Log file to use for all outputs (STDOUT, STDERR)
LOG_FILE=${LOG_DIR:-/tmp}/access.log

OK=0

# While no SIGTERM launch a server listening
while [ ${OK} -eq 0 ] ; do
	TOKEN="${TOKEN_PREFIX}$(rand-keygen -s ${SIZE})"
	echo "Server ${TOKEN} starting..." | tee -a ${LOG_FILE}

	# Start server listening with netcat
	nc -vlkp ${PORT} 2>&1 | tee -a ${LOG_FILE}
	OK=$?

	echo "Server ${TOKEN} closed" | tee -a ${LOG_FILE}
done
```

##### Build

```sh
docker build -t sample-test17 -f Dockerfile17 --build-arg RUN_SCRIPT=start_server.sh ./
```

##### Run

_Note: If the current user you are using to run docker commands is not **root**
you have to give write permission on the bound volume on host machine with 
`mkdir $(pwd)/logs && chmod -R 777 $(pwd)/logs`._

```sh
docker run -d --name test17 -v $(pwd)/logs:/var/log/nc:Z -p 8080:5000 -e TOKEN_NAME="Server_" sample-test17
```

_Note: We run this container in **detached** mode that does not open interactive
shell. The `--volume` or `-v` parameter make a binding for volumes between host
and container as value like \<HOST_RESOURCE\>:\<CONTAINER_RESOURCE\>._

Now you can see file **./logs/access.log**. Run following command to display
file content:
```sh
tail -f ./logs/access.log
```

Now if you connect from the host machine on port 8080 you will see information
about server in the file.

_Note: Volume parameter allow to pass option like `:z` – this add permissions
to all containers using label 'svirt_sandbox_file_t' – or `:Z` – this add
permissions only to the current container label – for volume permissions._

## 11. Conclusion

The instrcutions used in this tutorial are the most common used instructions
used to build an image in Docker. But you can see full instructions on the
[official documentation](https://docs.docker.com/engine/reference/builder/).


### Some usefull commands:

##### Remove all containers of this tutorial:
```sh
docker ps -a | grep -P "test\d{0,2}" | awk '{print $1}' | xargs docker rm -vf
```

##### Remove all images of this tutorial:
```sh
docker images -a | grep -P "sample-test\d{0,2}|<none>" | awk '{print $3}' | xargs docker rmi 2>/dev/null
```

