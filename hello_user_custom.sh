#!/bin/bash

# Set token name from TOKEN_NAME env variable or 'Token' by default
TOKEN_DESC=${TOKEN_NAME:-Token}

# Print 'Hello custom <USERNAME>
#        <TOKENNAME>: <RANDOMKEY>'
# With system user name got from whoami command
echo "Hello custom $(whoami)
${TOKEN_DESC}: $(rand-keygen -s $1)"
